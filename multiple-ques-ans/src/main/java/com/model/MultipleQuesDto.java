package com.model;

import java.util.List;


public class MultipleQuesDto {
    
	private List<QuesAnsEntity> quesAnsList;
    public List<QuesAnsEntity> getQuesAnsList() {
		return quesAnsList;
	}
    public void setQuesAnsList(List<QuesAnsEntity> quesAnsList) {
		this.quesAnsList = quesAnsList;
	}
    @Override
	public String toString() {
		return "MultipleQuesDto [quesAnsList=" + quesAnsList + "]";
	}
}
