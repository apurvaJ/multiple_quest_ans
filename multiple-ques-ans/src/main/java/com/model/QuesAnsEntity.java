package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ques_ans")
public class QuesAnsEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4447124588472245305L;

	@Id
    @GeneratedValue
    private Integer id;
	@Column(name="question")
	private String ques;
	@Column(name="answer")
	private String ans;
	@Column(name="ans_type_id")
	private Integer ansTypeId;
	@Column(name="parent_index")
	private Integer parentIndex;
	@Column(name="child_index")
	private Integer childIndex;
	@Column(name="batch_index")
	private Integer batchIndex;
	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public String getQues() {
		return ques;
	}

	public void setQues(String ques) {
		this.ques = ques;
	}

	
	public Integer getAnsTypeId() {
		return ansTypeId;
	}

	public void setAnsTypeId(Integer ansTypeId) {
		this.ansTypeId = ansTypeId;
	}

	
	public Integer getParentIndex() {
		return parentIndex;
	}

	public void setParentIndex(Integer parentIndex) {
		this.parentIndex = parentIndex;
	}

	public Integer getChildIndex() {
		return childIndex;
	}

	public void setChildIndex(Integer childIndex) {
		this.childIndex = childIndex;
	}
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBatchIndex() {
		return batchIndex;
	}

	public void setBatchIndex(Integer batchIndex) {
		this.batchIndex = batchIndex;
	}

	@Override
	public String toString() {
		return "QuesAnsEntity [id=" + id + ", ques=" + ques + ", ans=" + ans
				+ ", ansTypeId=" + ansTypeId + ", parentIndex=" + parentIndex
				+ ", childIndex=" + childIndex + ", batchIndex=" + batchIndex
				+ "]";
	}

	

	

	

		
}
