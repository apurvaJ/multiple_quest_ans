package com.service;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.model.QuesAnsEntity;

public interface IMultipleQuesAns {
	public void save(List<QuesAnsEntity> list);
	public   List<QuesAnsEntity>  getAllList() ;

}
