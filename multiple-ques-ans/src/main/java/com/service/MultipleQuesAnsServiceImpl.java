package com.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.IMultipleQuesAnsDao;
import com.model.MultipleQuesDto;
import com.model.QuesAnsEntity;

@Service
public class MultipleQuesAnsServiceImpl implements IMultipleQuesAns {

	@Autowired
	IMultipleQuesAnsDao questionAnswerDao;

	@Transactional
	public void save(List<QuesAnsEntity> list) {
		List<QuesAnsEntity> quesAnsList = new ArrayList<QuesAnsEntity>();
		Iterator<QuesAnsEntity> it = list.iterator();
		Integer batchIndex = questionAnswerDao.getMaxIndex();
		if (null == batchIndex) {
			batchIndex = 0;
		}
		while (it.hasNext()) {
			QuesAnsEntity entity = it.next();
			if (entity.getAnsTypeId() == 3) {
				String[] ans = entity.getAns().split(",");
				for (String string : ans) {
					QuesAnsEntity quesAnsEntity = new QuesAnsEntity();
					quesAnsEntity.setBatchIndex(batchIndex + 1);
					quesAnsEntity.setQues(entity.getQues());
					quesAnsEntity.setAns(string);
					quesAnsEntity.setAnsTypeId(entity.getAnsTypeId());
					quesAnsEntity.setParentIndex(entity.getParentIndex());
					quesAnsEntity.setChildIndex(entity.getChildIndex());
					quesAnsList.add(quesAnsEntity);
				}

			} else {
				entity.setBatchIndex(batchIndex + 1);
				quesAnsList.add(entity);
			}
		}
		questionAnswerDao.save(quesAnsList);
	}

	public List<QuesAnsEntity> getAllList() {
		List<QuesAnsEntity> list = questionAnswerDao.getAllList();
		return list;
	}

}
