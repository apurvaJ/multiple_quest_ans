package com.ctl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.model.MultipleQuesDto;
import com.model.QuesAnsEntity;
import com.service.IMultipleQuesAns;

@RestController
public class MultipleQuesAnsCtl {

	@Autowired
	IMultipleQuesAns multipleQuestionAnswerService;

	@RequestMapping(value = "/showForm")
	public ModelAndView show() {
		ModelAndView mav = new ModelAndView("multipleQuesAns");
		return mav;
	}

	@RequestMapping(value = "/saveQuestionAnswerData", method = RequestMethod.POST)
	public String saveQuestionAnswerData(MultipleQuesDto MultipleQuesDto) {
		multipleQuestionAnswerService.save(MultipleQuesDto.getQuesAnsList());
		String res = "Success";
		return res;
	}

	@RequestMapping(value = "/getAllList", method = RequestMethod.GET, produces = "application/json")
	public List<QuesAnsEntity> getAllList() {
		List<QuesAnsEntity> quesAnsList = multipleQuestionAnswerService.getAllList();
		return quesAnsList;
	}

}
