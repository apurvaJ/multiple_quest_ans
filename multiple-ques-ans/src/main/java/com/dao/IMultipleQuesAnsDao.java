package com.dao;

import java.util.List;

import com.model.QuesAnsEntity;

public interface IMultipleQuesAnsDao {
	public void save(List<QuesAnsEntity> list) ;
	public List<QuesAnsEntity> getAllList();
	public Integer getMaxIndex();
}
