package com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

import com.model.QuesAnsEntity;

public class MultipleQuesAnsDaoImpl implements IMultipleQuesAnsDao {

	@Autowired
	EntityManagerFactory entityManagerFactory;

	public void save(List<QuesAnsEntity> list) {
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();
		entityManager.getTransaction().begin();
		for (QuesAnsEntity quesAnsDto : list) {
			entityManager.persist(quesAnsDto);
		}
		entityManager.getTransaction().commit();

	}

	public List<QuesAnsEntity> getAllList() {
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();
		Query query = entityManager
				.createQuery(" select e from QuesAnsEntity e where parentIndex=childIndex OR childIndex is null");
		List<QuesAnsEntity> list = query.getResultList();
		return list;
	}

	public Integer getMaxIndex() {
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();
		Query query = entityManager
				.createQuery("select max(batchIndex) from QuesAnsEntity");
		Integer index = (Integer) query.getSingleResult();
		return index;
	}

}
