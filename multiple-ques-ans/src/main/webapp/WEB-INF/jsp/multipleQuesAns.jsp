<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"rel="stylesheet">
<link href="<c:url value="/resources/css/layout2.css" />"rel="stylesheet">

<style>
table {
	width: 100%;
}

.text-box {
	margin: 10px 0 0 20px;
	width: 90%;
	height: 60px;
	border: 1px solid #ccc;
}

.text {
	border: 1px solid #ccc;
	width: 70%;
}

.select {
	float: right
}

section {
	background-color: #eee;
	padding: 5px 0;
	margin-bottom: 15px;
	overflow: hidden;
}

.table1 section>div {
	margin-top: 5px;
}

.child {
	margin: 10px 0 0 20px;
	float: left;
	width: 100%;
}

input, textarea {
	border: 1px solid #ccc;
	border-radius: 4px;
	color: #555;
	font: 300 14px/22px "Roboto";
}
</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 ">
				<section class="wrapper">
					<h2 class="call">Add New Call</h2>
					<div id="containerBox">
						<form:form id="formContainerBox" action="saveQuestionAnswerData"
							method="post" modelAttribute="MultipleQuesDto">
							<div class="div1"></div>
							<div class="single-text-area" style="display: none;"></div>
							<div class="single-line" style="display: none;"></div>
							<div class="multi-line" style="display: none;"></div>
							<input type="hidden" id="row_index" />
						</form:form>
					</div>
					<div class="add-question">
						<button type="button" id="addButton">add new question</button>
					</div>
					<div class="bottomSec">
						<button type="submit" class="saveBtn">Save</button>
						<button type="reset" class="rstBtn">Cancel</button>
						<button type="button" class="lstBtn">List</button>


					</div>
				</section>
			</div>
		</div>
	</div>

	<script src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/script.js" />"></script>


</body>
</html>
